package oiasso.systems.examples.gestion.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan({ "oiasso.systems.examples.gestion.web.*" })
public class GestionConfig {
}
