package oiasso.systems.examples.gestion.web.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


public class GestionMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[] { GestionConfig.class, DataConfig.class };
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class[] { GestionWebMvc.class };
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/" };
  }

}
