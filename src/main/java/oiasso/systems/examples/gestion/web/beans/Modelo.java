package oiasso.systems.examples.gestion.web.beans;

public class Modelo {

  // *****************
  // *** ATRIBUTOS ***
  // *****************

  /** Numero del modelo */
  private String modelo;

  /** Ejercicio real del modelo */
  private Integer ejercicioReal;

  /** modl_ del modelo */
  private String modl_;

  /** modl_reg_activo del modelo */
  private String modl_reg_activo;

  // *******************
  // *** CONSTRUCTOR ***
  // *******************

  public Modelo() {
    super();
  }

  // ***********************
  // *** MÉTODOS GET/SET ***
  // ***********************

  public String getModelo() {
    return this.modelo;
  }

  public void setModelo(final String modelo) {
    this.modelo = modelo;
  }

  public Integer getEjercicioReal() {
    return this.ejercicioReal;
  }

  public void setEjercicioReal(final Integer ejercicioReal) {
    this.ejercicioReal = ejercicioReal;
  }

  public String getModl_() {
    return this.modl_;
  }

  public void setModl_(final String modl_) {
    this.modl_ = modl_;
  }

  public String getModl_reg_activo() {
    return this.modl_reg_activo;
  }

  public void setModl_reg_activo(final String modl_reg_activo) {
    this.modl_reg_activo = modl_reg_activo;
  }

}
