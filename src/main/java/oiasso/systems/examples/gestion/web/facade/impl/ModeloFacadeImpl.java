package oiasso.systems.examples.gestion.web.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import oiasso.systems.examples.gestion.web.beans.Modelo;
import oiasso.systems.examples.gestion.web.dao.ModeloDao;
import oiasso.systems.examples.gestion.web.facade.ModeloFacade;


@Service
public class ModeloFacadeImpl implements ModeloFacade {

  @Autowired
  private ModeloDao modeloDAO;

  @Override
  public Modelo getModelo(final String numeroModelo) {
    return null;
  }

  @Override
  public List<Modelo> getModelos() {
    return null;
  }

}
